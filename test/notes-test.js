import React from 'react'
import { expect } from 'chai'
import { describe, it } from 'mocha'
import { mount } from 'enzyme'
import Client from '../src/components/Client'
import Contract from '../src/components/Contract'
import store from '../src/lib/store'
import { logon } from '../src/actions/appActions'

// Im am testing the core functionality of the task (notes) with a
// feature-driven approach.

const testNotes = [
	{ message: 'This is a test note.', user: 'Adam' },
	{ message: 'This is another message from adam.', user: 'Adam' },
	{ message: 'This is a message by hugh.', user: 'Hugh' },
	{ message: 'This is a message by adam again.', user: 'Adam' }
]

function testNotesWith (Component) {
	it('displays notes that have been posted', () => {
		const context = { store }
		const wrapper = mount(Component, { context })
		const input = wrapper.find('textarea')

		testNotes.forEach(({ message, user }, index) => {
			store.dispatch(logon(user))
			input.node.value = message
			input.simulate('change', input)
			wrapper.find('form').simulate('submit')

			expect(wrapper.find('Note')).to.have.length(index + 1)
			expect(wrapper.find('[data-type="message"]').at(index).text()).to.equal(message)
			expect(wrapper.find('[data-type="user"]').at(index).text()).to.equal(user)
		})
	})
}

describe('<Client />', () => {
	testNotesWith(<Client routeParams={{ client: 'bank-of-america' }} />)
})

describe('<Contract />', () => {
	testNotesWith(<Contract routeParams={{ client: 'bank-of-america', contract: 'bank-of-america-1' }} />)
})
