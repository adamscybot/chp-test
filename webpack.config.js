let webpack = require('webpack')
let path = require('path')
let HtmlWebpackPlugin = require('html-webpack-plugin')
let env = process.env.NODE_ENV

var getPlugins = function (env) {
	const GLOBALS = {
		'process.env.NODE_ENV': JSON.stringify(env),
		__DEV__: env === 'development'
	}

	const plugins = [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin(GLOBALS),
		new HtmlWebpackPlugin({
			inject: false,
			title: 'Adam\'s CHP notes app',
			template: require('html-webpack-template'),
			appMountId: 'app'
		})
	]

	switch (env) {
		case 'production':
			plugins.push(new webpack.optimize.DedupePlugin())
			plugins.push(new webpack.optimize.UglifyJsPlugin())
			break
		case 'development':
			plugins.push(new webpack.HotModuleReplacementPlugin())
			plugins.push(new webpack.NoErrorsPlugin())
			break
	}

	return plugins
}

let getLoaders = (env) => (
	[
		{ test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel'], exclude: /node_modules/ },
		{ test: /\.json$/, loaders: ['json'] },
		{ test: /\.(png|jpg)$/, include: path.join(__dirname, 'src'), loader: 'url?limit=1024&name=images/[name]_[hash].[ext]' }
	]
)

let getEntry = (env) => {
	const entry = ['babel-polyfill']

	if (env === 'development') {
		entry.push('webpack-hot-middleware/client')
	}

	entry.push('./src/index')

	return entry
}

module.exports = {
	devtool: env === 'production' ? 'source-map' : 'cheap-source-map',
	entry: getEntry(env),
	target: env === 'test' ? 'node' : 'web',
	output: {
		path: path.join(__dirname, 'dist'),
		publicPath: '',
		filename: 'js/bundle.js'
	},
	plugins: getPlugins(env),
	module: {
		loaders: getLoaders(env)
	}
}
