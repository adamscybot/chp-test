// Tiny server with hot module replacement
// to make dev easier.

let browserSync = require('browser-sync')
let webpack = require('webpack')
let webpackDevMiddleware = require('webpack-dev-middleware')
let webpackHotMiddleware = require('webpack-hot-middleware')
let config = require('../webpack.config')

const bundler = webpack(config)

browserSync({
	port: 3020,
	ui: {
		port: 3021
	},
	server: {
		baseDir: 'dist',

		middleware: [
			webpackDevMiddleware(bundler, {
				publicPath: config.output.publicPath,
				stats: { colors: true }
			}),

			webpackHotMiddleware(bundler)
		]
	},

	files: [
		'dist/*.html'
	]
})
