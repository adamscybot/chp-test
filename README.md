CHP Test
========

This is my submission for the CHP coding test. It is a simple notes system, built using React and Redux. Everything is in ES2015+, and then transpiled using Babel (via Webpack).

For styles, I am using styled-components (very new), which allows for encapsulation (and automatic namespacing of class names) of the styles and logic in a single component file. For this reason, the CSS is viewed inside the component itself. This is the latest in CSS-in-JS solutions aimed to truly scope css to its relevant component only. CSS Modules would be an alternative.

Im using stateless components where possible for maximum cleanliness and performance.

I have thrown in some unit tests via Enzyme also. I like consistency in how the code looks, so I always use eslint as well.

I have decided not to use automated project setup tools like `create-react-app` in this instance as I wanted to demonstrate my understanding of webpack also.

Features
--------

* Timestamped notes attributed to users.
* **Bonus:** Initial "logon" screen which enables you to make comments under different "users". In reality this would of course be a logon screen with server side auth.
* **Bonus:**  Everything stored in local storage so your notes should stay after a refresh (instead of a server just for examples sake).
* **Bonus:**  Surrounding clients/contracts hierarchy with bootstrapped data to serve as an example of how the system might look & how it might be used. Powered by react-router.
* **Bonus:**  Validation.
* **Bonus:**  Unit test for the core notes functionality.


How to run
---------------

For the sake of being able to run this easily, I have included the build files in the `dist` folder. The actual source code can be viewed in the `src` folder.

Simply open `dist/index.html` in your browser.

Or, you can visit the hosted version [here](http://adam-thomas.co.uk/chp).

Test
--------

To run the unit tests simply

```
npm install
npm run test
```

Build
-----

If you want to build the source yourself you can simply:

```
npm install
npm run build
```

Developing
-----

To make my life easier during dev there is a tiny dev server which hot reloads everything. This is not a requirement in production.

```
npm install
npm run dev
```
