import { createSelector } from 'reselect'

const clientIdSelector = (state, props) => props.routeParams.client
const contractIdSelector = (state, props) => props.routeParams.contract
const clientsSelector = state => state.get('clients')
const contractsSelector = state => state.get('contracts')

export const clientSelector = createSelector(
  clientIdSelector,
	clientsSelector,
  (clientId, clients) => clients.find((client) => (client.get('id') === clientId))
)

export const contractsForClientSelector = createSelector(
  clientIdSelector,
	contractsSelector,
  (clientId, contracts) => contracts.filter((contract) => (contract.get('client') === clientId))
)

export const contractSelector = createSelector(
  contractIdSelector,
	contractsSelector,
  (contractId, contracts) => contracts.find((contract) => (contract.get('id') === contractId))
)
