import { createSelector } from 'reselect'

const clientIdSelector = (state, props) => props.routeParams.client
const contractIdSelector = (state, props) => props.routeParams.contract
const notesSelector = state => state.get('notes')

export const notesForClientSelector = createSelector(
  clientIdSelector,
	notesSelector,
  (clientId, notes) => notes.filter((note) => (note.get('item') === clientId && note.get('type') === 'client'))
)

export const notesForContractSelector = createSelector(
  contractIdSelector,
	notesSelector,
  (contractIdSelector, notes) => notes.filter((note) => (note.get('item') === contractIdSelector && note.get('type') === 'contract'))
)
