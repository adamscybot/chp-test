import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ImmutablePropTypes from 'react-immutable-proptypes'
import history from '../lib/history'
import styled from 'styled-components'

// I am using memoized selectors (reselect) in order to grab what the component needs.
// This keeps logic that searches for contracts/clients out of the view also.
import { clientSelector, contractsForClientSelector } from '../selectors/clientSelectors'
import { notesForClientSelector } from '../selectors/noteSelectors'

import { postNote } from '../actions/noteActions'

import BackLink from './BackLink'
import List from './List'
import Notes from './Notes'

function gotoContract (client, contract) {
	history.push({ name: 'contract', params: { contract, client } })
}

const Client = ({ contracts, client, notes, postNote }) => {
	const postNoteToClient = (message) => {
		postNote({
			message,
			type: 'client',
			item: client.get('id')
		})
	}

	return (
		<Container>
			<BackLink to={{ name: 'clients' }}>← Back to clients</BackLink>
			<List data={contracts} title={`Contracts for ${client.get('name')}`} onItemClick={(contract) => gotoContract(client.get('id'), contract)} />
			<Notes notes={notes} onAdd={postNoteToClient} />
		</Container>
	)
}

const idNameMapType = ImmutablePropTypes.mapContains({
	id: React.PropTypes.string.isRequired,
	name: React.PropTypes.string.isRequired
})

Client.propTypes = {
	contracts: ImmutablePropTypes.listOf(
		idNameMapType
	).isRequired,
	client: idNameMapType,
	notes: ImmutablePropTypes.list,
	postNote: React.PropTypes.func.isRequired
}

export default connect(
	(state, props) => ({
		contracts: contractsForClientSelector(state, props),
		client: clientSelector(state, props),
		notes: notesForClientSelector(state, props)
	}),
	(dispatch) => bindActionCreators({ postNote }, dispatch)
)(Client)

const Container = styled.section`
	width: 100%;
`
