import React from 'react'
import styled from 'styled-components'

const Input = ({ input, placeholder, meta: { error, touched } }) => (
	<span>
		<Field error={error && touched} placeholder={placeholder} {...input} />
	</span>
)

Input.propTypes = {
	input: React.PropTypes.object,
	placeholder: React.PropTypes.string,
	error: React.PropTypes.string,
	meta: React.PropTypes.shape({
		error: React.PropTypes.string,
		touched: React.PropTypes.bool
	})
}

export default Input

const Field = styled.input`
	border: 0;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	height: 40px;
	margin: 20px 0 0 0;
	width: 300px;
	outline: none;
	font-size: 13px;
	padding: 0 8px;
	border: 2px solid ${props => props.error ? 'red' : 'transparent'}
`
