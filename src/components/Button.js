import styled from 'styled-components'

const Button = styled.button`
	padding: 10px 20px;
	border: 0;
	background: ${props => props.theme.color3}
	color: #fff;
	font-size: 17px;
	cursor: pointer;
	outline: none; 

	&:hover {
		background: ${props => props.theme.color1}
	}
`

export default Button
