import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import styled from 'styled-components'

const List = ({ data, title, onItemClick }) => (
	<Container>
		{title && <Header>{title}</Header>}
		<Ul>
			{data.map((entry) => (
				<Item onClick={() => onItemClick(entry.get('id'))} key={entry.get('id')}>{entry.get('name')}</Item>
			))}
		</Ul>
	</Container>
)

List.propTypes = {
	data: ImmutablePropTypes.listOf(
		ImmutablePropTypes.contains({
			id: React.PropTypes.string.isRequired,
			name: React.PropTypes.string.isRequired
		})
	).isRequired,
	title: React.PropTypes.string,
	onItemClick: React.PropTypes.func
}

export default List

const Container = styled.div`
	width: 100%;
`

const Header = styled.h2`
	font-size: 30px;
	font-weight: bold;
	margin: 0 0 30px 0;
`

const Ul = styled.ul`
	list-style: none;
	margin: 0;
	padding: 0;
	width: 100%;
`

const Item = styled.li`
	cursor: pointer;
	font-size: 20px;
	padding: 20px;
	border-top: 1px solid ${props => props.theme.color1};
	width: 100%;

	&:last-child {
		border-bottom: 1px solid ${props => props.theme.color1};
	}

	&:hover {
		background: ${props => props.theme.color2};
	}
`
