import React from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import { injectGlobal } from 'styled-components'

// Provides the store everywhere via context
// and then leaves react-router to decide on what to render.
const Root = ({ store, history, routes }) => (
	<Provider store={store}>
		<Router history={history} routes={routes} />
	</Provider>
)

Root.propTypes = {
	store: React.PropTypes.object.isRequired,
	history: React.PropTypes.object.isRequired,
	routes: React.PropTypes.object.isRequired
}

export default Root

// Truly global styles (ie. fonts, body element stuff)
injectGlobal`
	html, body, #app {
		margin: 0;
		padding: 0;
		height: 100%;
		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
		box-sizing: border-box;
		line-height: 1;
	}

	*, *:before, *:after {
		box-sizing: inherit;
	}
`
