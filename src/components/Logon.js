import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Field, reduxForm } from 'redux-form/immutable'
import styled from 'styled-components'
import history from '../lib/history'

import { logon } from '../actions/appActions'

import Button from './Button'
import Input from './Input'

function validate (values) {
	const errors = {}

	if (!values.get('username')) {
		errors.username = 'You must enter a username to logon'
	}

	return errors
}

const Logon = ({ handleSubmit, logon }) => {
	const processLogon = (values) => {
		logon(values.get('username').toLowerCase())
		history.push({ name: 'clients' })
	}

	return (
		<CentreContainer>
			<Form onSubmit={handleSubmit(processLogon)}>
				<Header>Logon</Header>
				<Field placeholder='Enter a fake username' name='username' component={Input} />
				<Go type='submit'>Go</Go>
			</Form>
		</CentreContainer>
	)
}

Logon.propTypes = {
	handleSubmit: React.PropTypes.func,
	logon: React.PropTypes.func
}

const LogonWithForm = reduxForm({
	form: 'logon',
	touchOnBlur: false,
	validate
})(Logon)

export default connect(undefined, dispatch => (
	bindActionCreators({ logon }, dispatch))
)(LogonWithForm)

const CentreContainer = styled.div`
	flex: 1;
	align-items: center;
	justify-content: center;
	display: flex;
`

const Form = styled.form`
	background: ${props => props.theme.color2};
	padding: 30px;
	border-radius: 5px;
`

const Header = styled.h2`
	font-size: 40px;
	color: #fff;
	margin: 0;
`

const Go = styled(Button)`
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	height: 40px;
`
