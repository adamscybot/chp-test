import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import styled from 'styled-components'
import TimeAgo from 'react-timeago'

const Note = ({ note }) => (
	<NoteContainer>
		<Message><span data-type='message'>{note.get('message')}</span></Message>
		<InfoBar><div>Posted by <span data-type='user'>{note.get('user')}</span> <TimeAgo date={note.get('datetime')} /></div></InfoBar>
	</NoteContainer>
)

Note.propTypes = {
	note: ImmutablePropTypes.mapContains({
		message: React.PropTypes.string.isRequired,
		datetime: React.PropTypes.string.isRequired,
		user: React.PropTypes.string.isRequired
	}).isRequired
}

export default Note

const NoteContainer = styled.div`
	padding: 10px;
	padding: 0 0 20px 0;
`

const Message = styled.pre`
	padding: 15px;
	font-family: inherit;
	border: 1px solid ${props => props.theme.color2};
`

const InfoBar = styled.div`
	display: flex;
	justify-content: flex-end;
	font-style: italic;
	font-size: 12px;
`
