import React from 'react'
import styled, { ThemeProvider } from 'styled-components'

import Nav from './Nav'

const App = ({children}) => (
	<ThemeProvider theme={theme}>
		<Background>
			<Nav />
			<Container>
				{children}
			</Container>
		</Background>
	</ThemeProvider>
)

App.propTypes = {
	children: React.PropTypes.node
}

export default App

// The colours are passed through the context to all styled-components
const theme = {
	color1: 'rgba(102, 133, 134, 1)',
	color2: 'rgba(130, 174, 177, 1)',
	color3: 'rgba(147, 198, 214, 1)',
	color4: 'rgba(215, 206, 178, 1)',
	color5: 'rgba(165, 158, 140, 1)'
}

// Styled-components make extensive use of the tagged template literal
// feature of ES6. Essentially this is the same as styled.div(styles).
const Background = styled.div`
	min-height: 100%;
	background: #f0f0f0;
	display: flex;
	justify-content: center;
`

const Container = styled.div`
	padding: 100px 20px 20px 20px;
	flex: 1;
	display: flex;
	max-width: 900px;
`
