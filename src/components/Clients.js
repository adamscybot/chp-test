import React from 'react'
import { connect } from 'react-redux'
import ImmutablePropTypes from 'react-immutable-proptypes'
import history from '../lib/history'

import List from './List'

function gotoClient (client) {
	history.push({ name: 'client', params: { client } })
}

const Clients = ({clients}) => (
	<List data={clients} title='Clients' onItemClick={gotoClient} />
)

Clients.propTypes = {
	clients: ImmutablePropTypes.listOf(
		ImmutablePropTypes.contains({
			id: React.PropTypes.string.isRequired,
			name: React.PropTypes.string.isRequired
		})
	).isRequired
}

export default connect((state) => ({
	clients: state.get('clients')
}))(Clients)
