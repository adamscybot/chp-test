import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ImmutablePropTypes from 'react-immutable-proptypes'
import styled from 'styled-components'
import history from '../lib/history'

// I am using memoized selectors (reselect) in order to grab what the component needs.
// This keeps logic that searches for contracts/clients out of the view also.
import { contractSelector, clientSelector } from '../selectors/clientSelectors'
import { notesForContractSelector } from '../selectors/noteSelectors'

import { postNote } from '../actions/noteActions'

import BackLink from './BackLink'
import Notes from './Notes'

class Contract extends Component {

	static propTypes = {
		contract: ImmutablePropTypes.mapContains({
			name: React.PropTypes.string
		}).isRequired,
		notes: ImmutablePropTypes.list.isRequired,
		client: ImmutablePropTypes.mapContains({
			name: React.PropTypes.string
		}).isRequired,
		postNote: React.PropTypes.func.isRequired
	}

	onComponentWillMount () {
		// Redirect to correct pretty URL if client id does not match
		// client id of this contract
		let { contract, client } = this.props
		const clientId = contract.get('client')
		if (!client || clientId !== client.get('id')) {
			history.replace({ name: 'contract', params: { client: clientId, contract: contract.get('id') } })
		}
	}

	postNoteToContract = (message) => {
		let { postNote, contract } = this.props
		postNote({
			message,
			type: 'contract',
			item: contract.get('id')
		})
	}

	render () {
		let { contract, notes, client } = this.props

		return (
			<Container>
				<BackLink to={{ name: 'client', params: { client: client.get('id') } }}>← Back to {client.get('name')}</BackLink>
				<Header>{contract.get('name')}</Header>
				<Notes notes={notes} onAdd={this.postNoteToContract} />
			</Container>
		)
	}
}

export default connect(
	(state, props) => ({
		contract: contractSelector(state, props),
		notes: notesForContractSelector(state, props),
		client: clientSelector(state, props)
	}),
	(dispatch) => bindActionCreators({ postNote }, dispatch)
)(Contract)

const Container = styled.section`
	width: 100%;
`

const Header = styled.h2`
	font-size: 30px;
	font-weight: bold;
	margin: 0 0 30px 0;
`
