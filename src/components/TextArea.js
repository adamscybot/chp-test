import React from 'react'
import styled from 'styled-components'

const TextArea = ({ input, placeholder, meta: { error, touched } }) => (
	<span>
		<Field error={error && touched} placeholder={placeholder} {...input} />
		{error && touched && <Error>{error}</Error>}
	</span>
)

TextArea.propTypes = {
	input: React.PropTypes.object,
	placeholder: React.PropTypes.string,
	error: React.PropTypes.string,
	meta: React.PropTypes.shape({
		error: React.PropTypes.string,
		touched: React.PropTypes.bool
	})
}

export default TextArea

const Field = styled.textarea`
	width: 100%;
	border: 1px solid ${props => props.theme.color1};
	height: 200px;
	padding: 20px;
	font-size: 15px;
	outline: none;
	border: 2px solid ${props => props.error ? 'red' : props.theme.color1};
`

const Error = styled.div`
	color: red;
	font-size: 12px;
	margin: 10px 0;
`
