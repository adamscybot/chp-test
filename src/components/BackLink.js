import styled from 'styled-components'
import { Link } from 'react-router'

const BackLink = styled(Link)`
	color: black;
	margin: 0 0 20px 0;
	display: block;
`

export default BackLink
