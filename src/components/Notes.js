import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import { Field, reduxForm } from 'redux-form/immutable'
import styled from 'styled-components'

import TextArea from './TextArea'
import Button from './Button'
import Note from './Note'

function validate (values) {
	const errors = {}
	const message = values.get('message')

	if (!message || message.length < 10) {
		errors.message = 'You must enter a note with more than 10 characters'
	}

	return errors
}

// Notes is a dumb component. The logic that adds the notes to the store
// in is in the parent component. This allows this to be easily repurposed
// for both clients and contracts.
const Notes = ({ notes, onAdd, handleSubmit, reset }) => {
	const submit = (values) => {
		onAdd(values.get('message'))
		reset()
	}

	return (
		<Container>
			<Header>Notes</Header>
			<Ul>
				{notes.size === 0 ? (
					<span>No notes posted for this item</span>
				) : notes.map((note) => (
					<li key={note.get('id')}><Note note={note} /></li>
				))}
			</Ul>
			<AddContainer onSubmit={handleSubmit(submit)}>
				<Header>Add a note</Header>
				<Field placeholder='Enter your note here...' name='message' component={TextArea} />
				<ButtonRow>
					<Button type='submit'>Post</Button>
				</ButtonRow>
			</AddContainer>
		</Container>
	)
}

Notes.propTypes = {
	notes: ImmutablePropTypes.listOf(
		ImmutablePropTypes.contains({
			id: React.PropTypes.number.isRequired,
			user: React.PropTypes.string.isRequired,
			type: React.PropTypes.string.isRequired,
			message: React.PropTypes.string.isRequired,
			item: React.PropTypes.string.isRequired,
			datetime: React.PropTypes.string.isRequired
		})
	).isRequired,
	onAdd: React.PropTypes.func.isRequired,
	handleSubmit: React.PropTypes.func.isRequired,
	reset: React.PropTypes.func.isRequired
}

export default reduxForm({
	form: 'logon',
	touchOnBlur: false,
	validate
})(Notes)

const Container = styled.section`
	width: 100%;
	margin: 30px 0 0 0;
`
const Header = styled.h2`
	font-size: 20px;
	font-weight: bold;
	margin: 0 0 30px 0;
`

const Ul = styled.ul`
	list-style: none;
	margin: 0;
	padding: 0;
	width: 100%;
`

const AddContainer = styled.form`
	width: 100%;
	margin: 30px 0 0 0;
`

const ButtonRow = styled.div`
	display: flex;
	justify-content: flex-end;
	padding: 15px 0 0 0;
`
