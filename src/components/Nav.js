import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styled from 'styled-components'

import { logout } from '../actions/appActions'

const Nav = ({username, logout}) => (
	<NavContainer>
		<Title>CHP</Title>
		{username && <div>
			Welcome {username}.
			<Logout onClick={logout}>Logout</Logout>
		</div>}
	</NavContainer>
)

Nav.propTypes = {
	username: React.PropTypes.string,
	logout: React.PropTypes.func.isRequired
}

export default connect((state) => ({
	username: state.get('username')
}), (dispatch) => bindActionCreators({ logout }, dispatch))(Nav)

const NavContainer = styled.nav`
	height: 60px;
	position: fixed;
	top: 0;
	width: 100%;
	box-shadow: 1px 2px 11px rgba(0,0,0,0.5);
	background: ${props => props.theme.color3};
	display: flex;
	align-items: center;
	padding: 0 30px;
	justify-content: space-between;
	color: #fff;
`

const Title = styled.h1`
	font-size: 20px;
	margin: 0;
`

const Logout = styled.button`
	text-decoration: underline;
	background: transparent;
	border: 0;
	color: #fff;
	font-size: inherit;
	cursor: pointer;
	padding: 0 0 0 10px;
`
