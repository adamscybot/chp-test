// I am using redux to store notes and associated items
// such as clients and contracts

import { createStore, applyMiddleware, compose } from 'redux'
import { combineReducers } from 'redux-immutable'
import { reducer as formReducer } from 'redux-form/immutable'
import ReduxThunk from 'redux-thunk'
import Immutable from 'immutable'
import { autoRehydrate } from 'redux-persist-immutable'
import * as appReducers from '../reducers/appReducers'
import * as clientReducers from '../reducers/clientReducers'
import * as noteReducers from '../reducers/noteReducers'

const notesApp = combineReducers({ form: formReducer, ...appReducers, ...clientReducers, ...noteReducers })

const enhancers = [
	applyMiddleware(ReduxThunk),
	autoRehydrate()
]

if (window.__REDUX_DEVTOOLS_EXTENSION__) {
	enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__())
}

const store = createStore(
	notesApp,
	Immutable.Map(),
	compose(
		...enhancers
	)
)

export default store
