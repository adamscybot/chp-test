// This is where the route definitions are held.

import React from 'react'
import { Route, IndexRoute } from 'react-router'
import store from './store'

import App from '../components/App'
import Logon from '../components/Logon'
import Clients from '../components/Clients'
import Client from '../components/Client'
import Contract from '../components/Contract'

// We dont want to allow access to internal pages
// unless the username has been set.
function requireAuth (nextState, replace) {
	if (!store.getState().get('username')) {
		replace({ name: 'logon' })
	}
}

export default (
	<Route path='/' component={App}>
		<Route name='logon' path='logon' component={Logon} />
		<IndexRoute name='clients' component={Clients} onEnter={requireAuth} />
		<Route name='client' path='client/:client' component={Client} onEnter={requireAuth} />
		<Route name='contract' path='client/:client/contract/:contract' component={Contract} onEnter={requireAuth} />
	</Route>
)
