import routes from './routes'
import createHashHistory from 'history/lib/createHashHistory'
import { useRouterHistory } from 'react-router'
import useNamedRoutes from 'use-named-routes'

// I am using hash history for compatibility with the file protocol

const history = useNamedRoutes(useRouterHistory(createHashHistory))({ routes })

export default history
