export const POST_NOTE = 'POST_NOTE'

export function postNote (note) {
	return (dispatch, getState) => {
		// We can autoamtically attach the user and datetime
		// metadata from data that is already available in the store.
		note.user = getState().get('username')
		note.datetime = (new Date()).toISOString() // Caveat: This would really be set on the server
		dispatch({
			type: POST_NOTE,
			payload: note
		})
	}
}
