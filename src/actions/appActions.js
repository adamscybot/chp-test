import history from '../lib/history'

export const LOGON = 'LOGON'
export const LOGOUT = 'LOGOUT'

export function logon (username) {
	return {
		type: LOGON,
		payload: username
	}
}

export function logout () {
	history.push({ name: 'logon' })
	return {
		type: LOGOUT
	}
}
