import Immutable from 'immutable'
import { POST_NOTE } from '../actions/noteActions'

export function notes (state = Immutable.List(), action) {
	switch (action.type) {
		case POST_NOTE:
			const { type, user, datetime, item, message } = action.payload

			// We want a unique ID so find the largest one and increment.
			// similiar to auto keys in sql db's
			const noteWithMaxId = state.maxBy(note => note.get('id'))
			const id = noteWithMaxId ? noteWithMaxId.get('id') + 1 : 1
			return state.push(Immutable.Map({ id, type, user, datetime, item, message }))
		default:
			return state
	}
}
