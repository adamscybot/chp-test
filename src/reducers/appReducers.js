import { LOGON, LOGOUT } from '../actions/appActions'

export function username (state = null, action) {
	switch (action.type) {
		case LOGON:
			return action.payload
		case LOGOUT:
			return null
		default:
			return state
	}
}
