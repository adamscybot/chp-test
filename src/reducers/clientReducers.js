import Immutable from 'immutable'
import clientsData from '../lib/data/clients.json'
import contractsData from '../lib/data/contracts.json'

// I am bootstrapping some hard coded client/contract data for examples sake.
// I am using this boilerplate to put this in the store so that in the future
// you could easily add add/remove functionality for clients/contracts.

export function clients (state = Immutable.fromJS(clientsData), action) {
	return state
}

export function contracts (state = Immutable.fromJS(contractsData), action) {
	return state
}
