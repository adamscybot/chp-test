import React from 'react'
import { render } from 'react-dom'
import { persistStore } from 'redux-persist-immutable'
import store from './lib/store'
import history from './lib/history'
import routes from './lib/routes'
import Root from './components/Root'

const props = { store, history, routes }

// We only want to render the app once the initial state
// has been restored from localstorage
persistStore(store, {}, () => {
	render((
		<Root {...props} />
	), document.getElementById('app'))
})
