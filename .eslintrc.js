module.exports = {
	"parser": "babel-eslint",
	"extends": ["standard", "standard-react"],
	"plugins": [
		"standard",
		"react"
	],
	"rules": {
		"indent": [2, "tab", {"SwitchCase": 1}],
		"no-tabs": 0,
		"react/jsx-indent": [2, "tab"]
	}
};
